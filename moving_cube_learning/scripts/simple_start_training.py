#!/usr/bin/env python

import gym
# ROS packages required
import rospy
# import our training environment
from openai_gazebo.moving_cube import one_disk_walk

if __name__ == '__main__':

    rospy.init_node('movingcube_gym', anonymous=True, log_level=rospy.INFO)
    # Create the Gym environment
    env = gym.make('MovingCubeOneDiskWalk-v0')
    rospy.loginfo("Gym environment done")
    env.close()
